import 'dart:math';
import 'package:flutter/material.dart';
import 'package:makes_pace_app/pages/page_index.dart';

class PageMakeFace extends StatefulWidget {
  const PageMakeFace({
    super.key,
    required this.friendName
  });

  final String friendName;

  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}

class _PageMakeFaceState extends State<PageMakeFace> {

  List eyebrow = ['assets/eyebrow_1.png','assets/eyebrow_2.png','assets/eyebrow_3.png'];
  List eyes = ['assets/eyes_1.png','assets/eyes_2.png','assets/eyes_3.png'];
  List face = ['assets/face_1.png','assets/face_2.png'];
  List mouse = ['assets/mouse_1.png','assets/mouse_2.png','assets/mouse_3.png'];
  List nose = ['assets/nose_1.png','assets/nose_2.png','assets/nose_3.png'];

  int random = Random().nextInt(3);
  int randoms = Random().nextInt(2);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('결과'),
      ),
      body: Column(
        children: [
          Center(
            child: Stack(
              children: [
                Image.asset(eyebrow[random]),
                Image.asset(eyes[random]),
                Image.asset(face[randoms]),
                Image.asset(mouse[random]),
                Image.asset(nose[random]),
              ],
            ),
          ),
          Center(child:
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                  '친구 이름 : ',
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                  widget.friendName,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          ),
          Center(child: OutlinedButton(
            onPressed: () {
              Navigator.of(context).pop(MaterialPageRoute(builder: (context) => PageIndex()));
            },
           child: Text('종료'),
          ),
          )
        ],
      ),
    );
  }
}